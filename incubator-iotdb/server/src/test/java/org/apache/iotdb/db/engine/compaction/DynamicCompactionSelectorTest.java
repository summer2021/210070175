/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.apache.iotdb.db.engine.compaction;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.iotdb.db.conf.IoTDBDescriptor;
import org.apache.iotdb.db.constant.TestConstant;
import org.apache.iotdb.db.engine.compaction.dynamic.DynamicCompactionTsFileManagement;
import org.apache.iotdb.db.engine.storagegroup.TsFileResource;
import org.apache.iotdb.db.exception.StorageEngineException;
import org.apache.iotdb.db.exception.metadata.MetadataException;
import org.apache.iotdb.tsfile.exception.write.WriteProcessException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DynamicCompactionSelectorTest extends LevelCompactionTest {

  File tempSGDir;

  @Before
  public void setUp() throws IOException, WriteProcessException, MetadataException {
    super.setUp();
    tempSGDir = new File(TestConstant.BASE_OUTPUT_PATH.concat("tempSG"));
    tempSGDir.mkdirs();
  }

  @After
  public void tearDown() throws IOException, StorageEngineException {
    super.tearDown();
    FileUtils.deleteDirectory(tempSGDir);
  }

  /**
   * select all
   */
  @Test
  public void testCompactionSelectAll() throws NoSuchFieldException, IllegalAccessException {
    long prevQueryTimeInterval = IoTDBDescriptor.getInstance().getConfig().getQueryTimeInterval();
    IoTDBDescriptor.getInstance().getConfig().setQueryTimeInterval(100000);
    DynamicCompactionTsFileManagement dynamicCompactionTsFileManagement = new DynamicCompactionTsFileManagement(
        COMPACTION_TEST_SG, tempSGDir.getPath());
    dynamicCompactionTsFileManagement.addAll(seqResources, true);
    dynamicCompactionTsFileManagement.addAll(unseqResources, false);
    Field fieldForkedSequenceTsFileResources = DynamicCompactionTsFileManagement.class
        .getDeclaredField("forkedSequenceTsFileResources");
    fieldForkedSequenceTsFileResources.setAccessible(true);

    dynamicCompactionTsFileManagement.forkCurrentFileList(0);
    List<TsFileResource> forkedSequenceTsFileResources = (List<TsFileResource>) fieldForkedSequenceTsFileResources
        .get(dynamicCompactionTsFileManagement);
    assertEquals(6, forkedSequenceTsFileResources.size());

    IoTDBDescriptor.getInstance().getConfig().setQueryTimeInterval(prevQueryTimeInterval);
  }

  /**
   * select none
   */
  @Test
  public void testCompactionSelectNone() throws NoSuchFieldException, IllegalAccessException {
    long prevQueryTimeInterval = IoTDBDescriptor.getInstance().getConfig().getQueryTimeInterval();
    IoTDBDescriptor.getInstance().getConfig().setQueryTimeInterval(1);
    DynamicCompactionTsFileManagement dynamicCompactionTsFileManagement = new DynamicCompactionTsFileManagement(
        COMPACTION_TEST_SG, tempSGDir.getPath());
    dynamicCompactionTsFileManagement.addAll(seqResources, true);
    dynamicCompactionTsFileManagement.addAll(unseqResources, false);
    Field fieldForkedSequenceTsFileResources = DynamicCompactionTsFileManagement.class
        .getDeclaredField("forkedSequenceTsFileResources");
    fieldForkedSequenceTsFileResources.setAccessible(true);

    dynamicCompactionTsFileManagement.forkCurrentFileList(0);
    List<TsFileResource> forkedSequenceTsFileResources = (List<TsFileResource>) fieldForkedSequenceTsFileResources
        .get(dynamicCompactionTsFileManagement);
    assertEquals(0, forkedSequenceTsFileResources.size());

    IoTDBDescriptor.getInstance().getConfig().setQueryTimeInterval(prevQueryTimeInterval);
  }
}
